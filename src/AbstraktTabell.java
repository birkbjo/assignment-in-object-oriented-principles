import java.util.Iterator;

interface AbstraktTabell<T> {
	public boolean settInn(T objekt, int index);
	public T finn(int index);
	public Iterator<T> iterator();
}
