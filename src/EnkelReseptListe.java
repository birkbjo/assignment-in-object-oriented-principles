import java.util.Iterator;

public class EnkelReseptListe implements Iterable<Resept>{
	protected Node forste, siste;
	public int antall;
	
	public EnkelReseptListe() {
		Node lh = new Node(null);
		forste = lh;
		siste = lh;
		antall = 0;
	}
	protected class Node {
		Node neste;
		Resept obj;
		Node(Resept t) {
			obj = t;
		}
	}
	
	public void settInn(Resept resepten) {
		Node n = new Node(resepten);
		n.neste = forste.neste;
		forste.neste = n;
		if (siste.neste == n) {
		    siste = n;
		}
		antall++;
	}
	
	public Resept finn(int reseptNr) {
		Node noden = forste.neste;
		for(int i = 0;i < antall;i++) {
			if(noden.obj.hentReseptNr() == reseptNr) {
				return noden.obj;
			} else {
				noden = noden.neste;
			}
		}
		throw new IllegalArgumentException("Reseptnummer ble ikke funnet");
	}

	public Iterator<Resept> iterator() {
		return new listeIterator<Resept>();
		
	}
	//Iterator for lenkelista
		 private class listeIterator<Resept> implements Iterator<Resept> {
			Node current = forste.neste;
			
			public boolean hasNext() {
				return current != null;
			}

			public Resept next() {
				Node tmp = current;
				current = current.neste;
				return (Resept) tmp.obj;
			}

			public void remove() {	
				//ikke implementert
			}
		 }
}

class EldstForstReseptListe extends EnkelReseptListe {
	int antall = 0;
	
	public void settInn(Resept resepten) {
		Node noden = new Node(resepten);
		siste.neste = noden;
		siste = noden;
		antall++;
	}
}

class YngstForstReseptListe extends EnkelReseptListe {

	
}