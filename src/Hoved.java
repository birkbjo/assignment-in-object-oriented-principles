import java.util.Scanner;
import java.util.InputMismatchException;

class Hoved {
	private Data innlesing = new Data();
	private Scanner input = new Scanner(System.in, "UTF-8");
	private Tabell<Person> personTabell = innlesing.lesPerson();
	private Tabell<Legemiddel> legemiddelTabell = innlesing.lesLegemiddel();
	private SortertEnkelListe<Lege> legeListe = innlesing.lesLege();
	private EnkelReseptListe reseptListe = innlesing.lesResept();

	// Starter programmet og skriver ut meny
	void startMeny() {
		int valgt = 0;
		System.out
		.println("Angi et tall mellom 1 og 7 etterfulgt av enter, avhengig av operasjonen du vil utfore.");
		valgt = 0;
		while (valgt != 7) {
			skrivMeny();
			System.out.print("Valg: ");
			valgt = lesTall();

			switch (valgt) {

			case 1:
				nyLegemiddel();
				break;
			case 2:
				nyLege();
				break;
			case 3:
				nyPerson();
				break;
			case 4:
				nyResept();
				break;
			case 5:
				inputHentLegemiddel();
				break;
			case 6:
				statistikkMeny();
				break;
			case 7:
				System.out.println("Avsluttet!");
				break;
			default:
				System.out
				.println("Angi et tall mellom 1 og 7, avhengig av operasjonen du vil utfore.");
			}
		}
	}

	private void skrivMeny() {
		String wrap = "*****************************************";
		System.out.println(wrap);
		System.out.println("*****HOVEDMENY*****");
		System.out.println("* 1. Opprett nytt legemiddel");
		System.out.println("* 2. Opprett ny lege");
		System.out.println("* 3. Opprett ny person");
		System.out.println("* 4. Opprett ny resept");
		System.out.println("* 5. Hent legemiddel paa resept");
		System.out.println("* 6. Statistikk");
		System.out.println("* 7. Avslutt");
		System.out.println(wrap);
	}

	private void statistikkMeny() {
		int valgt = 0;
		System.out
		.println("Angi et tall mellom 1 og 6 etterfulgt av enter, avhengig av operasjonen du vil utfore.");
		valgt = 0;
		while (valgt != 5) {
			skrivStatistikkMeny();
			System.out.print("Valg: ");
			valgt = lesTall();
			switch (valgt) {

			case 1:
				statistikkAlt();
				enterFortsett();
				break;
			case 2:
				statistikkPersResept();
				enterFortsett();
				break;
			case 3:
				statistikkAvtaleLege();
				enterFortsett();
				break;
			case 4:
				statistikkPersonerResept();
				enterFortsett();
				break;
			case 5:
				break; // Tilbake til meny
			case 6:
				System.out.println("Avsluttet!");
				System.exit(0);
			default:
				System.out
				.println("Angi et tall mellom 1 og 6, avhengig av operasjonen du vil utfore.");
			}
		}
	}

	private void skrivStatistikkMeny() {
		String wrap = "*****************************************";
		System.out.println(wrap);
		System.out.println("*****STATISTIKKMENY*****");
		System.out.println("1. Skriv ut data om legemidler, leger og personer");
		System.out.println("2. Oversikt over en persons gyldige blaa resepter");
		System.out.println("3. Oversikt over avtaleleger sine resepter");
		System.out
		.println("4. Oversikt over personene sine gyldige vanedannende resepter");
		System.out.println("5. Tilbake til hovedmeny");
		System.out.println("6. Avslutt");
		System.out.println(wrap);
	}

	/*
	 * Metode for aa trykke enter for aa fortsette Brukes etter lange utskrifter
	 * for at brukeren skal ha oversikt over all utskriften
	 */
	private void enterFortsett() {
		System.out.println("[Trykk enter for aa fortsette til meny]...");
		if (input.hasNextLine())
			input.nextLine();
		input.nextLine();
	}

	// Opprett ny person ved aa sporre brukeren

	/*
	 * Opprett ny resept ved aa sporre brukeren. Hvis det skjer noe feil under
	 * oppretting, saa avslutter metoden og gaar tilbake til menyen, slik at det
	 * kan lages en ny lege hvis det onskes.
	 */
	void nyResept() {
		System.out.print("Skriv navnet paa legen som skriver ut resepten: ");
		input.nextLine();
		String navn = input.nextLine().trim();
		Lege legen = legeListe.finn(navn);
		if (legen != null)
			System.out.println("Legen ble funnet");
		else {
			System.out
			.println("Legen ble ikke funnet. Prov aa opprett en ny lege.");
			return;
		}
		System.out.println("Skriv nummeret til legemiddelet: ");
		int legemiddelNr = lesTall();
		Legemiddel middelet = legemiddelTabell.finn(legemiddelNr);
		if (middelet == null) {
			System.out.println("Legemiddelet ble ikke funnet med det nummeret");
			return;
		}
		System.out.println("Skriv inn personnummeret til personen: ");
		int persnr = lesTall();
		Person personen = personTabell.finn(persnr);
		if (personen == null) {
			System.out.println("Det finnes ingen personer med det nummeret");
			return;
		}
		System.out.println("Skriv antall ganger resepten kan brukes (reit)");
		int reit = lesTall();
		System.out.println("Skriv fargen paa resepten (hvit/blaa)");
		System.out.print("H / B : ");
		String farge = validerInput("h", "b").toLowerCase();
		Resept resepten = new Resept(middelet, legen, persnr, reit, farge);
		reseptListe.settInn(resepten);
		personen.leggTilResept(resepten);
		legen.skrivUtResept(resepten);
		System.out.println(resepten.skrivUtFarge()
				+ " resept med legemiddelet " + middelet.hentNavn()
				+ " ble skrevet ut til " + personTabell.finn(persnr).hentNavn()
				+ "\n av legen " + navn + ". Reit : " + reit);
	}



	// Metoden brukes for aa sporre brukeren om mengden legemiddelet skal ha.
	private int sporMengde(String form) {
		if (form.equals("P"))
			System.out.println("Hvor mange piller er det i en eske?");
		if (form.equals("I"))
			System.out.println("Hvor mye virkemiddel er det i en dose?");
		if (form.equals("L"))
			System.out.println("Hvor mange cm3 inneholder legemiddelet?");

		int mengde = 0;
		System.out.print("Mengde: ");
		try {
			mengde = input.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Du skrev ikke inn et tall, prov igjen. ");
			input.nextLine();
		}
		return mengde;
	}

	// Iterer gjennom og skriver ut info om legemidler, leger og personer
	void statistikkAlt() {
		String wrap = "*********************";
		System.out.println("Informasjon om legemidler: ");
		for (Legemiddel test : legemiddelTabell) {
			if (test != null) {
				System.out.println(wrap);
				test.skrivUtAlt();
				System.out.println(wrap);
			}
		}
		System.out.println("Informasjon om leger: ");
		for (Lege lege : legeListe) {
			if (lege != null) {
				System.out.println(wrap);
				lege.skrivUtAlt();
				System.out.println(wrap);
			}
		}
		System.out.println("Informasjon om personer: ");
		for (Person person : personTabell) {
			if (person != null) {
				System.out.println("Navn: " + person.hentNavn());
				System.out.println("Kjonn: " + person.hentKjonn());
				System.out.println(wrap);
			}
		}
	}

	void statistikkPersResept() {
		System.out.println("Skriv inn personnummeret: ");
		System.out.print("Nr: ");
		int persNr = lesTall();
		Person personen = personTabell.finn(persNr);
		YngstForstReseptListe pResepter = personen.hentReseptListe();
		int antall = 0;
		// int dose = 0;
		for (Resept r : pResepter) {
			if (r.hentFarge().equalsIgnoreCase("b") && r.reit > 0) {
				System.out.println("Resept " + r.hentReseptNr()
						+ " med legemiddelet " + r.hentLegemiddel().hentNavn()
						+ " har " + r.reit + " doser igjen.");
				antall++;
			}
		}
		if (antall > 0) {
			System.out.println("Personen har totalt " + antall
					+ " gyldige blaa resepter.");
		} else {
			System.out.println("Personen har ingen gyldige blaa resepter.");
		}
	}

	void statistikkAvtaleLege() {
		int antallNarko = 0;
		for (Lege l : legeListe) {
			if (l instanceof AvtaleLege) {
				System.out.println("Navn: " + l.hentNavn());
				EldstForstReseptListe legeReseptListe = l.hentReseptListe();
				for (Resept r : legeReseptListe) {
					if (r.hentLegemiddel().hentType().equals("A - narkotisk")) {
						antallNarko++;
					}
				}
				System.out.println("Har skrevet ut " + antallNarko
						+ " narkotiske legemidler.");
				System.out.println("*******************");
			}
		}
	}

	void statistikkPersonerResept() {

		int totalt = 0;
		int totaltMenn = 0;
		int totaltKvinne = 0;
		for (Person p : personTabell) {
			if (p != null) {
				int antallVane = 0;
				System.out.println("Navn: " + p.hentNavn());
				YngstForstReseptListe personReseptListe = p.hentReseptListe();
				for (Resept r : personReseptListe) {
					if (r.hentLegemiddel().hentType()
							.equals("B - vanedannende")
							&& r.reit > 0) {
						antallVane++;
					}
				}// end for resept
				totalt += antallVane;
				if (p.hentKjonn().equals("M"))
					totaltMenn += antallVane;
				System.out.println("Har " + antallVane
						+ " gyldige resepter paa vanedannene legemidler");
				System.out.println("*******************");
			}// end nullcheck
		}
		totaltKvinne = totalt - totaltMenn;
		System.out.println("Totalt: " + totalt
				+ " gyldige resepter paa vanedannende legemidler.");
		System.out.println("Fordelt paa " + totaltMenn + " Menn og "
				+ totaltKvinne + " kvinner.");
	}

	void nyPerson() {
		if (input.hasNextLine())
			input.nextLine();
		System.out.print("Skriv navnet paa personen: ");
		String navn = input.nextLine().trim();
		System.out.println("Hvilket kjonn er personen? (mann/kvinne)");
		System.out.print("m/k:");
		String kjonn = input.nextLine().trim().toUpperCase();

		while (!kjonn.equals("K") && !kjonn.equals("M")) {
			System.out.print("Prov igjen: ");
			kjonn = input.nextLine().trim().toUpperCase();
		}
		int persNr = personTabell.hentIndeks();
		personTabell.settInn(new Person(navn, kjonn), persNr);
		System.out.println("Personen " + navn
				+ " ble lagt til med personnummer " + persNr + ".");
	}

	/*
	 * Kontrollmetode for input Sorger for at brukeren skriver inn et tall
	 * Skrevet slik at exception-behandlig ikke er nodvendig.
	 */
	private int lesTall() {
		int tallet;
		while (!input.hasNextInt()) {
			System.out.println("Ikke et gyldig tall, prov igjen");
			input.next();
		}
		tallet = input.nextInt();
		return tallet;
	}

	private String validerInput(String alt1, String alt2) {
		// if(input.hasNextLine()) input.next();
		String inn = input.next().trim();
		while (!inn.equalsIgnoreCase(alt1) && !inn.equalsIgnoreCase(alt2)) {
			System.out.print("Prov igjen: ");
			inn = input.next().trim();
		}
		return inn;
	}

	/*
	 * Oppretter et nytt legemiddel Bruker flere hjelpemetoder for aa dele opp
	 * koden Spesielt lagLegemiddel metodene er viktige. Bruker flere sjekker
	 * for aa hindre at brukeren skriver feil, og hjelper brukeren til aa
	 * opprette legemiddelet.
	 */
	private void nyLegemiddel() {
		int pris = 0;
		int styrke = 0;
		input.nextLine();
		System.out
		.println("Er legemiddelet A - narkotisk, B - vandedannende eller C - vanlig?");
		System.out.print("Skriv inn A,B eller C: ");
		String type = input.nextLine().trim().toUpperCase();

		while (!type.equals("A") && !type.equals("B") && !type.equals("C")) {
			System.out.print("Prov igjen: ");
			type = input.nextLine().trim().toUpperCase();
		}

		System.out.println("Skriv inn navnet paa legemiddelet: ");
		System.out.print("Navn: ");
		String navn = input.nextLine().trim();
		System.out
		.println("P - pille, I - injeksjon eller L - liniment(salve)");
		System.out.print("Skriv inn P, I eller L: ");
		String form = input.nextLine().trim().toUpperCase();

		while (!form.equals("P") && !form.equals("I") && !form.equals("L")) {
			System.out.print("Prov igjen: ");
			form = input.nextLine().trim().toUpperCase();
		}

		System.out.println("Skriv inn prisen paa legemiddelet: ");
		System.out.print("kr: ");
		pris = lesTall();
		int mengde = sporMengde(form);
		// Sjekk narkotisk, vanedannende eller vanlig, og deretter spor om
		// styrke
		if (type.equals("A") || type.equals("B")) {
			if (type.equals("A")) {
				System.out.println("Hvor sterkt narkotisk er legemiddelet?");
			}
			if (type.equals("B")) {
				System.out.println("Hvor vanedannende er legemiddelet?");
			}
			System.out.print("Styrke: ");
			styrke = lesTall();
			if (type.equals("A"))
				lagLegemiddelA(navn, form, pris, styrke, mengde);
			if (type.equals("B"))
				lagLegemiddelB(navn, form, pris, styrke, mengde);
		} else {
			lagLegemiddelC(navn, form, pris, mengde);

		}
	}

	/*
	 * Henter et legemiddel basert paa personnummeret til en person og nummeret
	 * til resepten. Nummeret til resepten er nummeret paa resepten i personens
	 * reseptbeholder Skriver deretter ut info om resepten og legemiddelet.
	 */
	
	void inputHentLegemiddel() {
		System.out
		.println("Skriv inn personnummeret til personen som skal hente");
		System.out.print("Nr : ");
		int persNr = lesTall();
		Person personen = personTabell.finn(persNr);
		if (personen == null) {
			return;
		}
		System.out.println("Skriv inn nummeret til resepten: ");
		System.out.print("Nr : ");
		int reseptNr = lesTall();
		YngstForstReseptListe persResept = personen.hentReseptListe();
		if (persResept.antall == 0) {
			System.out.println("Personen har ingen resepter.");
			return;
		}
		Resept resepten = persResept.finn(reseptNr);
		if (resepten == null) {
			return;
		}
		int reit = resepten.reit;
		if (reit <= 0) {
			System.out.println("Resepten er ugyldig");
			return;
		}
		System.out.println("Info om resepten: ");
		System.out.println("Reseptnr: " + reseptNr);
		System.out.println("Farge : " + resepten.skrivUtFarge());
		System.out.println("Reit (igjen): " + --resepten.reit);
		System.out.println("Lege: " + resepten.hentLege().hentNavn());
		System.out.println("Person: " + personen.hentNavn());
		System.out.println("Info om legemiddelet: ");
		resepten.hentLegemiddel().skrivUtAlt();
		int pris = 0;
		if (resepten.hentFarge().equals("h"))
			pris = resepten.hentLegemiddel().hentPris();
		System.out.println("Legemiddelet ble hentet. " + pris
				+ ",- skal betales. ");
	}
	
	// Metodene under brukes under oppretting av legemidler(nylegemiddel() ved
	// aa sporre brukeren,
	
	void lagLegemiddelA(String navn, String form, int pris, int styrke,	int mengde) {
		int index = legemiddelTabell.hentIndeks();
		if (form.equals("P")) {
			legemiddelTabell.settInn(new LegemiddelAPille(navn, pris, styrke,
					mengde), index);
		}
		if (form.equals("I")) {
			legemiddelTabell.settInn(new LegemiddelAInjeksjon(navn, pris,
					styrke, mengde), index);
		}
			
		if (form.equals("L")) {
			legemiddelTabell.settInn(new LegemiddelALiniment(navn, pris,
					styrke, mengde), index);
		}
	}
	
	void lagLegemiddelB(String navn, String form, int pris, int styrke,	int mengde) {
		int index = legemiddelTabell.hentIndeks();
		if (form.equals("P")) {
			legemiddelTabell.settInn(new LegemiddelBPille(navn, pris, styrke,
					mengde), index);
		}
			
		if (form.equals("I")) {
			legemiddelTabell.settInn(new LegemiddelBInjeksjon(navn, pris,
					styrke, mengde), index);
		}
			
		if (form.equals("L")) {
			legemiddelTabell.settInn(new LegemiddelBLiniment(navn, pris,
					styrke, mengde), index);
		}	
	}
	void lagLegemiddelC(String navn, String form, int pris, int mengde) {
		int index = legemiddelTabell.hentIndeks();
		if (form.equals("P")) {
			legemiddelTabell.settInn(new LegemiddelCPille(navn, pris, mengde),
					index);
		}
			
		if (form.equals("I")) {
			legemiddelTabell.settInn(new LegemiddelCInjeksjon(navn, pris,
					mengde), index);
		}
			
		if (form.equals("L")) {
			legemiddelTabell.settInn(
					new LegemiddelCLiniment(navn, pris, mengde), index);
		}	
	}
	
	private void nyLege() {
		int spesialist = 0;
		int avtalenr = 0;
		if (input.hasNextLine())
			input.nextLine();
		System.out.print("Skriv navnet paa legen: ");
		String navn = input.nextLine().trim();
		System.out.println("Er " + navn + " en spesialist?");
		System.out.print("j/n:");
		String jaNei = input.nextLine().trim().toLowerCase();

		while (!jaNei.equals("j") && !jaNei.equals("n")) {
			System.out.print("Prov igjen: ");
			jaNei = input.nextLine().trim().toLowerCase();
		}

		System.out.println("Er " + navn + " en avtalelege?");
		System.out.print("j/n:");
		String avtale = input.nextLine().trim().toLowerCase();

		while (!avtale.equals("j") && !avtale.equals("n")) {
			System.out.print("Prov igjen: ");
			avtale = input.nextLine().trim().toLowerCase();
		}

		if (jaNei.equals("j")) {
			spesialist = 1;
		}
			
		if (avtale.equals("j")) {
			System.out.println("Skriv avtalenummeret til legen: ");
			avtalenr = lesTall();
		}

		if (avtale.equals("j")) {
			legeListe.settInn(new AvtaleLege(navn, spesialist, avtalenr));
		}
			
		else {
			legeListe.settInn(new Lege(navn, spesialist));
		}

		System.out.println("Legen " + navn + " ble lagt til.");
	}
}