import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class Data {
	
	File file = new File("data.txt");
	Tabell<Person> persontabell =new Tabell<Person>(100);
	Tabell<Legemiddel> legemiddeltabell=new Tabell<Legemiddel>(100);
	SortertEnkelListe<Lege> legeListe= new SortertEnkelListe<Lege>();
	EnkelReseptListe reseptListe=new EnkelReseptListe();
	
	void lesData(){

	}
	
	public Tabell<Person> lesPerson(){
		try {
            Scanner scanner = new Scanner(file,"UTF-8");
            scanner.nextLine(); //hopper over forste linje
            while (scanner.hasNextLine()) {
              String line=scanner.nextLine();               
               if(line.equals("")) break;
               String[] words=line.split(", ");
               int indeks = persontabell.hentIndeks();
               persontabell.settInn(new Person(words[1],words[2]),indeks);
            }
            scanner.close();
	  } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		return persontabell;
	}
	
	public Tabell<Legemiddel> lesLegemiddel(){	
		boolean legemidler=false;
		try {
            Scanner scanner = new Scanner(file,"UTF-8");
            String line=scanner.nextLine(); 
            while (scanner.hasNextLine()) {          	
            line=scanner.nextLine(); 
             if(line.startsWith("# Legemidler")){
            	 line = scanner.nextLine();
            	  legemidler=true;
            	//  continue;
              }
              
              if(legemidler){

            	 if(line.equals("")) break;
            	 String[] words=line.split(", ");
            	 int indeks=Integer.parseInt(words[0]); 
            	 int pris=Integer.parseInt(words[4]);
            	 int styrke = 0;
            	 if(words.length == 7) styrke=Integer.parseInt(words[6]);
            	 int mengde=Integer.parseInt(words[5]);
            	 if(words[2].equals("pille")){     
            		 if(words[3].equals("a")){
            			 legemiddeltabell.settInn(new LegemiddelAPille(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("b")){
            	 		legemiddeltabell.settInn(new LegemiddelBPille(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("c")){
            	 		legemiddeltabell.settInn(new LegemiddelCPille(words[1],pris,mengde),indeks);
            	 	}            	             	           	 
            	 }
            	 if(words[2].equals("injeksjon")){          		           	 
            		 if(words[3].equals("a")){
            			 legemiddeltabell.settInn(new LegemiddelAInjeksjon(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("b")){
            	 		legemiddeltabell.settInn(new LegemiddelBInjeksjon(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("c")){
            	 		legemiddeltabell.settInn(new LegemiddelCInjeksjon(words[1],pris,mengde),indeks);
            	 	}            	             	           	 
            	 }
            	 if(words[2].equals("liniment")){          		           	 
            		 if(words[3].equals("a")){
            			 legemiddeltabell.settInn(new LegemiddelALiniment(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("b")){
            	 		legemiddeltabell.settInn(new LegemiddelBLiniment(words[1],pris,styrke,mengde),indeks);
            	 	}else if(words[3].equals("c")){
            	 		legemiddeltabell.settInn(new LegemiddelCLiniment(words[1],pris,mengde),indeks);
            	 	}            	             	           	 
            	 }
            	 
               }
              }              
            scanner.close();                                
	  } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		return legemiddeltabell;
	}
	public SortertEnkelListe<Lege> lesLege(){	
		boolean lege=false;
		try {
            Scanner scanner = new Scanner(file,"UTF-8");
            
            while (scanner.hasNextLine()) {
            	String line=scanner.nextLine();  
            	if(line.startsWith("# Leger")){
              	  lege=true;
              	  continue;
                }
            	if(lege){
            	if(line.equals("")) break;
            	String[] words=line.split(", ");
            	String navn=words[0];
            	int spesialist=Integer.parseInt(words[1]);
            	int avtaleNr=Integer.parseInt(words[2]);
            	if(avtaleNr != 0) legeListe.settInn(new AvtaleLege(navn,spesialist,avtaleNr));
            	else legeListe.settInn(new Lege(navn, spesialist));
            	
            	}
            } //end while
            scanner.close();
	  } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		return legeListe;
	
	}
	
	public EnkelReseptListe lesResept() {				
		boolean resept=false;
		try {
            Scanner scanner = new Scanner(file,"UTF-8");
            
            while (scanner.hasNextLine()) {
            	String line=scanner.nextLine();  
            	if(line.startsWith("# Resepter")){
              	  resept=true;
              	  continue;
                }   
            	if(resept){
            		if(line.equals("")) break;
            		String[] words=line.split(", ");
            		String farge=words[1];
            		int persNr=Integer.parseInt(words[2]);
            		String legeNavn = words[3];
            		int legemiddelNr=Integer.parseInt(words[4]);
            		int reit=Integer.parseInt(words[5]);
            		Lege legen = legeListe.finn(legeNavn);
            		Person personen = persontabell.finn(persNr);
            		Resept resepten = new Resept(legemiddeltabell.finn(legemiddelNr), legen,persNr, reit, farge);
            		legen.skrivUtResept(resepten);
            		personen.leggTilResept(resepten);
            		reseptListe.settInn(resepten);
            	}
            }
            scanner.close();
	  } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
		return reseptListe;
	}
	
}