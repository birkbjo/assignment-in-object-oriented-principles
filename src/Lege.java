interface Avtale {
	int hentAvtalenr();
}

class Lege implements Comparable<Lege>,Lik {
	private String navn;
	private boolean spesialist;
	private EldstForstReseptListe reseptListe = new EldstForstReseptListe();
	Lege(String navn,int spesialist) {
		this.navn = navn;
		if(spesialist == 0) this.spesialist = false;
		else this.spesialist = true;
	}
	
	public boolean samme(String navn) {
		if (navn.equalsIgnoreCase(this.navn)) return true;
		return false;
	}

	
	public int compareTo(Lege lege) {
		return this.navn.compareTo(lege.navn);
	}
	
	public void skrivUtResept(Resept resepten) {
		reseptListe.settInn(resepten);
	}
	
	public String hentNavn() {
		return navn;
	}
	
	public boolean hentSpesialist() {
		return spesialist;
	}
	
	public EldstForstReseptListe hentReseptListe() {
		return reseptListe;
	}
	
	public void skrivUtAlt() {
		String spes = "Nei";
		if(spesialist) spes = "Ja";
		System.out.println("Navn: " +navn);
		System.out.println("Spesialist: " +spes);
	}
}

class AvtaleLege extends Lege implements Avtale{
	private int avtaleNr = 0;
	
	AvtaleLege(String navn, int spesialist,int avtaleNr) {
		super(navn,spesialist);
		this.avtaleNr = avtaleNr;
	}
	
	public int hentAvtalenr() {
		return avtaleNr;
	}
	
	public void skrivUtAlt() {
		super.skrivUtAlt();
		System.out.println("Avtalenummer: " + avtaleNr);
	}
}