
abstract class Legemiddel {
	private String navn;
	static private int nr = 0;
	private int pris;
	private int middelNr = 0;
	Legemiddel(String navn, int pris) {
		this.navn = navn;
		this.pris = pris;
		this.middelNr = nr;
		nr++;
	}
	
	public String hentNavn() {
		return navn;
	}
	
	public int hentPris() {
		return pris;
	}
	
	public void skrivUtAlt() {
		System.out.println("Navn : " + navn);
		System.out.println("Pris : " + pris);
		System.out.println("Nr : " + middelNr);
		System.out.println("Type : " + hentType());
		System.out.println("Form : " + hentForm());
		System.out.print("Mengde : ");
		skrivMengde();
		
	}
	
	abstract public String hentType();
	abstract public String hentForm();
	abstract public void skrivMengde();
}


