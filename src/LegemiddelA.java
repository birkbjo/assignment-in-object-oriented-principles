abstract class LegemiddelA extends Legemiddel {
	private int narkotisk;
	LegemiddelA(String navn,int pris, int styrke) {
		super(navn,pris);
		this.narkotisk = styrke;
	}
	
	public String hentType() {
		return "A - narkotisk";
	}
	
	public void skrivUtAlt() {
		super.skrivUtAlt();
		System.out.println("Narkotisk styrke: " + narkotisk);
	}
	
}

class LegemiddelAInjeksjon extends LegemiddelA implements Injeksjon{
	private int mengde;
	LegemiddelAInjeksjon(String navn,int pris, int styrke,int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Injeksjon";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " mg i en dose.");
	}
}

class LegemiddelAPille extends LegemiddelA implements Pille{
	private int mengde;
	LegemiddelAPille(String navn,int pris, int styrke,int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Pille";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " piller i en eske.");
	}
}

class LegemiddelALiniment extends LegemiddelA implements Liniment {
	private int mengde;
	
	LegemiddelALiniment(String navn,int pris, int styrke,int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Liniment";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " cm3 i en tube.");
	}
}