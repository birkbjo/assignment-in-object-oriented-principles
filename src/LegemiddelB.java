abstract class LegemiddelB extends Legemiddel {
	private int styrke;
	LegemiddelB(String navn,int pris, int styrke) {
		super(navn,pris);
		this.styrke = styrke;
	}
	
	public String hentType() {
		return "B - vanedannende";
	}
	
	public void skrivUtAlt() {
		super.skrivUtAlt();
		System.out.println("Vandedannende styrke: " + styrke);
	}
}
	
class LegemiddelBInjeksjon extends LegemiddelB implements Injeksjon{
	private int mengde;
	LegemiddelBInjeksjon(String navn,int pris, int styrke,int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Injeksjon";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " mg i en dose");
	}
}

class LegemiddelBPille extends LegemiddelB implements Pille{
	private int mengde;
	LegemiddelBPille(String navn,int pris, int styrke, int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Pille";
	}
	public void skrivMengde() {
		System.out.println("" + mengde + " piller i en eske");
	}
}

class LegemiddelBLiniment extends LegemiddelB implements Liniment {
	private int mengde;
	
	LegemiddelBLiniment(String navn,int pris, int styrke,int mengde) {
		super(navn,pris,styrke);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Liniment";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " cm3 i en tube.");
	}
}