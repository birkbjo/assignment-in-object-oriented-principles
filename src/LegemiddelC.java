abstract class LegemiddelC extends Legemiddel {
	LegemiddelC(String navn,int pris) {
		super(navn,pris);
	}
	public String hentType() {
		return "C - vanlig";
	}
	abstract public String hentForm();
}

class LegemiddelCInjeksjon extends LegemiddelC implements Injeksjon{
	private int mengde;
	LegemiddelCInjeksjon(String navn,int pris,int mengde) {
		super(navn,pris);
		this.mengde = mengde;
	}
	public int hentMengde() {
		return mengde;
	}
	public String hentForm() {
		return "Injeksjon";
	}
	
	public void skrivMengde() {
		System.out.println("" + mengde + " mg i en dose.");
	}
}

class LegemiddelCPille extends LegemiddelC implements Pille{
	private int mengde;
	LegemiddelCPille(String navn,int pris,int mengde) {
		super(navn,pris);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	public String hentForm() {
		return "Pille";
	}
	public void skrivMengde() {
		System.out.println("" + mengde + " piller i en eske.");
	}
}

class LegemiddelCLiniment extends LegemiddelC implements Liniment {
	private int mengde;
	
	LegemiddelCLiniment(String navn,int pris,int mengde) {
		super(navn,pris);
		this.mengde = mengde;
	}
	
	public int hentMengde() {
		return mengde;
	}
	
	public String hentForm() {
		return "Liniment";
	}
	public void skrivMengde() {
		System.out.println("" + mengde + " cm3 i en tube.");
	}
}