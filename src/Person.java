
class Person {
	private YngstForstReseptListe reseptListe = new YngstForstReseptListe();
	private String navn;
	private static int nummer = 0;
	private String kjonn;
	private int pnummer;
	Person(String navn, String kjonn) {
		this.navn = navn;
		this.kjonn = kjonn.toUpperCase();
		pnummer = nummer;
		nummer++;
	}
	public String hentNavn() {
		return navn;
	}

	public String hentKjonn() {
		return kjonn;
	}
	
	public void leggTilResept(Resept resepten) {
		reseptListe.settInn(resepten);
	}
	
	public YngstForstReseptListe hentReseptListe() {
		return reseptListe;
	}
}