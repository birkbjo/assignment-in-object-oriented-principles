
class Resept {
	static private int nr = 0;
	private Legemiddel legemiddel;
	private int persnr;
	public int reit;
	private String farge;
	private Lege lege;
	private int reseptnr;
	Resept(Legemiddel legemiddel, Lege lege, int persnr, int reit,String farge) {
		this.legemiddel = legemiddel;
		this.lege = lege;
		this.persnr = persnr;
		this.reit = reit;
		this.farge = farge.toLowerCase();
		this.reseptnr = nr;
		nr++;
	}
	
	public int hentReseptNr() {
		return reseptnr;
	}
	
	
	public String hentFarge() {
		return farge;
	}
	
	public Lege hentLege() {
		return lege;
	}
	
	public Legemiddel hentLegemiddel() {
		return legemiddel;
	}
	public String skrivUtFarge() {
		if(farge.equalsIgnoreCase("h")) return "Hvit";
		return "Blaa";
	}
}
