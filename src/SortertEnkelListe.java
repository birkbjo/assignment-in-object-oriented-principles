import java.util.Iterator;

interface AbstraktSortertEnkelListe<T> {
	void settInn(T objekt);
	T finn(String key);
	Iterator<T> iterator();
}

class SortertEnkelListe<T extends Comparable<T> & Lik> implements AbstraktSortertEnkelListe<T>,Iterable<T> {
	private Node forste, siste;
	private int antall;
	
	public SortertEnkelListe() {
		Node lh = new Node(null);
		forste = lh;
		siste = lh;
		antall = 0;
	}
	private class Node {
		Node neste;
		T obj;
		Node(T t) {
			obj = t;
		}
	}
	/*Setter inn et objekt i lenkelista i sortet rekkefolge
	Sjekker om lista er tom, og legger inn objektet forst 
	(etter listehodet) hvis den er det.
	Hvis det er noe i lista, sjekkes det om det nye objektet skal 
	vaere forann det forste objektet i listen .
	Hvis ikke, fortsett nedover lista og sjekk om den tilhoerer der.
	
	*/
	public void settInn(T objekt) {
		Node n = new Node(objekt);
		
		if(forste.neste == null) {
			forste.neste = n;
			siste = n;
			antall++;
		} else if((objekt.compareTo(forste.neste.obj) < 0)) {
			n.neste = forste.neste;
			forste.neste = n;
			if(siste.neste == n) {
				siste = n;
			}
			antall++;
		} else {
			Node forann = forste.neste;
			Node etter = forann.neste;
			while(etter != null) {
				if (objekt.compareTo(etter.obj) < 0) {
					break;
				}
				forann = etter;
				etter = forann.neste;
			}
			n.neste = forann.neste;
			forann.neste = n;
			if(siste.neste == n) {
				siste = n;
			}
			antall++;
		} 
	}

	public T finn(String key) {
		Node objekt = forste.neste;
		for(int i = 0;i < antall;i++) {
			if(objekt.obj.samme(key)) return objekt.obj;
			else objekt = objekt.neste;
		}
		return null;
	}

	public Iterator<T> iterator() {
		return new listeIterator<T>();
	}
	
	//Iterator for lenkelista
	 private class listeIterator<V> implements Iterator<V> {
		Node current = forste.neste;
		
		public boolean hasNext() {
			return current != null;
		}

		public V next() {
			Node tmp = current;
			current = current.neste;
			return (V) tmp.obj;
		}

		public void remove() {	
			//ikke implementert
		}
	}
	
	public void testUt() {
		Node n = forste.neste;
		while (n != null) {
			System.out.println(n.obj);
			n = n.neste;
		}
		System.out.println("Siste objekt er: " + siste.obj);
	}
	
}
