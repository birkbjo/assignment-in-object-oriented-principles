import java.util.Iterator;

class Tabell<T> implements AbstraktTabell<T>, Iterable<T>{
	private T[] arrayet;
	private int antall = 0;
	Tabell(int lengde) {
		arrayet = (T[]) new Object[lengde];
	}
	
	public boolean settInn(T objekt, int index) {
		if(arrayet[index] == null) {
			arrayet[index] = objekt;
			antall++;
			return true;
		}
		return false;
	}
	
	public T finn(int index) {
		try {
			if(arrayet[index] != null) {
			return arrayet[index];
			} 
		} catch(IndexOutOfBoundsException  e) {
			System.out.println("Ugyldig nummer.");
		}
		return null;
		
	}
	
	public Iterator<T> iterator() {
		return new TabellIterator<T>();
	}
	
	public int hentIndeks() {
		return antall;
	}
	//Iterator:
	private class TabellIterator<V> implements Iterator<V> {
		int size = arrayet.length;
		int currentIndex = 0;
		
		public boolean hasNext() {
			return currentIndex < size;
		}

		public V next() {
			return (V) arrayet[currentIndex++];
		}

		public void remove() {	
		}
	} //end indreklasse
}
